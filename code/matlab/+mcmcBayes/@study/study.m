classdef study
    %STUDY
    %   This extensible class, mcmcBayes.study, contains functions useful to performing studies and is also a 
    % data structure with information about either a single sequence or a group of sequences, whose results can
    % be generated together and presented.  Custom child study classes can handle general functions necessary for 
    % supporting a specific study.  Children of this class should be located in an external repository that is 
    % dedicated to the custom study.
    
    properties (Constant, Access = private) %define as private so that subclasses can have their own instances with same name
        version='1.0';  % API version for this class
    end

    properties        
        className       % type of the study (string equivalent of the class name, e.g., regressionEvaluation)
        uuid            % unique identifier for the study
        sequences       % array of sequence objects for the study
    end

    methods (Static)
        function retStudy=readStructFromXMLFile(xmlfile, myrepos)
            %readStructFromXMLFile 
            %   Load a study object from *.xml. 
            % xmlfile-study xmlfile name (either absolute or relative path inside repos's config directory)
            % myrepos (optional)-myrepos object with base dir and list of repositories
            stdout=1;
            fprintf(stdout, 'Loading %s for the study\n',xmlfile);

            doc = xmlread(xmlfile);
            %fprintf(1,'Parsed %s\n',xmlfile);
            %javaaddpath(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.javaDir));
            %call our java hacks method to insert the DOCTYPE element
            entityname='study';
            %     xmlStr=XmlHacks.insert(doc, entityname, dtdfile);%call Java function  (XmlHacks must be on javaclasspath)
            %     %now parse the hacked xml
            %     doc = XmlHacks.xmlreadstring(xmlStr);

            root=doc.getDocumentElement();
            root.normalize();%remove empty text nodes from this element and combine adjacent text nodes into a single text node
            rootname=root.getNodeName();
            if ~(strcmp(rootname,entityname)==1)
                error('Root element should be entityname');
            end

            %check version attribute
            tversion=cast(root.getAttribute('version'),'char');
            if (strcmp(mcmcBayes.study.version,tversion)~=1)
                error('Study version does not match in *.xml');
            end

            %get uuid and type attributes
            retStudy.uuid=cast(root.getAttribute('uuid'),'char');
            retStudy.className=strcat('mcmcBayes.',cast(root.getAttribute('className'),'char'));

            %iterate through the root element nodes
            rootNodeList=root.getChildNodes();
            for (i=1:rootNodeList.getLength())
                tNode=rootNodeList.item(i-1);
                
                if (strcmp("sequences",tNode.getNodeName())==1)
                    tNodeList=tNode.getChildNodes();
                    count=0;
                    for (j=1:tNodeList.getLength())
                        tNode2=tNodeList.item(j-1);
                        if (strcmp("sequence",tNode2.getNodeName())==1)
                            count=count+1;
                            fprintf(stdout, 'Loading sequence count #%d\n',count);
                            retStudy.sequences(count,1)=mcmcBayes.sequence.readStructFromXMLNode(tNode2, myrepos);  
                        end
                    end
                end
            end    
        end

        function retObj=loadAndConstructStudy(xmlfile, myrepos)
            %loadAndConstructStudy 
            %   Helper function that calls readStructFromXMLFile and constructor to create a new study object.
            % xmlfile-study xmlfile name (either absolute or relative path inside repository directory)
            % myrepos (optional)-myrepos object with base dir and list of repositories
            setStudy=mcmcBayes.study.readStructFromXMLFile(xmlfile, myrepos);
			retObj=mcmcBayes.study.constructor(setStudy);
        end

        function retObj=constructor(setStudy)
            %CONSTRUCTOR
            %   Helper function that uses setCfgStudy.type to call the appropriate class constructor for that type
            stdout=1;
    		fprintf(stdout, 'Constructing the study object\n');

            studyConstructorHandle=mcmcBayes.getFunctionHandle(setStudy.className,'constructor');
            studyHandle=mcmcBayes.getClassHandle(setStudy.className);
            if isempty(studyConstructorHandle)
                retObj=studyConstructorHandle(setStudy);
            else               
                %general constructor
                numSequences=size(setStudy.sequences,1);
                for i=1:numSequences                   
                    setSequences(i,1)=mcmcBayes.sequence.constructor(setStudy.sequences(i));
                end
    
                retObj=studyHandle(setStudy.uuid, setSequences);
            end
        end     

        function [retobj]=load(fullpath)
            load(fullpath,'archive');
            retobj=archive;
        end        

        function retver=getVersion()%this allows us to read the private constant
            retver=mcmcBayes.study.version;
        end
    end        

    methods
        function obj = study(setClassName, setUuid, setSequences)
            %STUDY 
            %   Construct an instance of this class
 
            obj.className=setClassName;
            obj.uuid=setUuid;
            obj.sequences=setSequences;
        end

        function retObj=runStudy(obj, sequenceIds)   
            stdout=1;
            fprintf(stdout,'Running the sequences in the study\n');
            if ~exist('sequenceIds','var')
                numSequences=size(obj.sequences,1);
                for i=1:numSequences
                    fprintf(stdout,'Running sequence %d\n',i);
                    obj.sequences(i)=obj.sequences(i).runSequence();
                    fprintf(stdout,'Completed running sequence %d\n',i);
                end
            else
                for id=1:length(sequenceIds)
                    i=sequenceIds(id);
                    fprintf(stdout,'Running sequence %d\n',i);
                    obj.sequences(i)=obj.sequences(i).runSequence();
                    fprintf(stdout,'Completed running sequence %d\n',i);
                end
            end

            fprintf(stdout,'Completed running the sequences in the study\n'); 
            retObj=obj;
        end

        function quickLook(obj)
            stdout=1;
            numSequences=size(obj.sequences,1);
            for i=1:numSequences
                fprintf(stdout,'Sequence %d\n',i);
                fprintf(stdout,'\tProject=%s/%s\n',obj.sequences(i).simulations(1).hSamplerInterfaces.dirProject, ...
                    obj.sequences(i).simulations(1).hSamplerInterfaces.fileProject);
                obj.sequences(i).simulations(1).variables.quickLook();
                obj.sequences(i).simulations(1).data.quickLook();
            end
        end

        function clearAll(obj)
            obj.sequences(1).clearAll();
        end

        function save(obj, fullpath)
            archive=obj;
            save(fullpath,'archive');
        end    
    end

    methods %getter/setter functions
        function obj=set.sequences(obj, setSequences)
            if ~(isa(setSequences,'mcmcBayes.sequence'))
                error('sequences is not mcmcInterface.sequence object');
            end            
            obj.sequences=setSequences;
        end

        function obj=set.className(obj, setType)
            if isempty(which(setType))
                error('type %s is not found on MATLAB path',setType);
            end
            obj.className=setType;
        end           
    end    
end


function handle=getFunctionHandle(strClass,strFunction)
    if (exist(strClass)==8)%is it a valid class on the path
        evalStr=sprintf('handle=@%s.%s;',strClass,strFunction);%construct the expression to get the handle
        eval(evalStr);%get the handle
    else
        handle=[];%return empty
    end
end
classdef sequence < matlab.mixin.Heterogeneous
    %SEQUENCE 
    %   This extensible class, mcmcBayes.sequence, contains functions and a data structure for groups of simulations
    % that are related and require special handling as a group (e.g., temperature runs in a power-posterior 
    % sequence).  Child sequence classes are either basic (a.k.a., vanilla) or custom, where the latter handle 
    % general functions necessary for supporting a specific sequence.  An example for custom sequence would be 
    % running a power-posterior set of simulations on a linear regression model to estimate the marginal likelihood
    % of the model given the data.  Children of this class should be located in an external repository that is
    % dedicated to the custom sequence.
    %
    % This is an abstract base class that can be instantiated with different child classes.  Therefore, since 
    % mcmcBayes.study contains an array of sequences that could potentially be different classes, 
    % mcmcBayes.sequence is a sub-class of matlab.mixin.Heterogeneous (an abstract class that supports forming 
    % heterogeneous arrays).

    properties (Constant, Access = private) %define as private so that subclasses can have their own instances with same name
        version='1.0';  % API version for this class
    end

    properties
        className       % name for the sequence's class (string equivalent of the class name, e.g., mcmcBayes.basicSequence)
        uuid            % unique identifier for the sequence
        hModel          % model object for the sequence
        simulations     % array of simulation objects for the sequence
    end

    methods (Abstract)
        retObj = runSequence(obj);
    end    

    methods (Static)
        function setSequence=readStructFromXMLNode(rootNode, myrepos)     
            %readStructFromXMLFile 
            %   Load a sequence object from *.xml.
            % xmlfile-sequence xmlfile name (either absolute or relative path inside repos's config directory)
            % myrepos (optional)-myrepos object with base dir and list of repositories

            stdout=1;
            fprintf(stdout, '\tLoading the sequence from rootNode\n');
            
            entityname='sequence';
            rootname=rootNode.getNodeName();
            if ~(strcmp(rootname,entityname)==1)
                error('Root element should be entityname');
            end

            %check version attribute
            tversion=cast(rootNode.getAttribute('version'),'char');
            if (strcmp(mcmcBayes.study.getVersion(),tversion)~=1)
                error('Sequence version does not match in *.xml');
            end

            %get uuid and type attributes
            setSequence.uuid=cast(rootNode.getAttribute('uuid'),'char');
            setSequence.className=strcat('mcmcBayes.',cast(rootNode.getAttribute('className'),'char'));
            
            for i=1:rootNode.getLength()
                tNode=rootNode.item(i-1);
                if (strcmp(tNode.getNodeName,'simulations')==1) %get simulations
                    simulationsNode=tNode;
                    simCount=1;
                    for j=1:simulationsNode.getLength()
                        tNode2=simulationsNode.item(j-1);
                        if (strcmp(tNode2.getNodeName,'simulation')==1)
                            simulationNode=tNode2;
                            setSequence.simulations(simCount,1)=mcmcInterface.simulation.readStructFromXMLNode(simulationNode, myrepos);
                            simCount=simCount+1;
                        end
                    end
                elseif (strcmp(tNode.getNodeName,'model')==1)
                    warning('may need to pass myrepos to readStructFromXMLNode')
                    setSequence.model=mcmcBayes.model.readStructFromXMLNode(tNode);
                elseif (strcmp(tNode.getNodeName,'#text')==1)
                    continue;%ignore
                elseif (strcmp(tNode.getNodeName,'#comment')==1)
                    continue;%ignore
                else %assume that the field is sequence subclass specific
                    handle=mcmcBayes.getFunctionHandle(setSequence.className,'updateStructFromXMLNode');
                    setSequence=handle(tNode,setSequence);
                end
            end 
        end

        function retObj=constructor(setSequence)
            %CONSTRUCTOR
            %   Helper function that uses setCfgSequence.type to call the appropriate class constructor for that type
            stdout=1;
            fprintf(stdout, 'Constructing a sequence object\n');

            sequenceConstructorHandle=mcmcBayes.getFunctionHandle(setSequence.className,'constructor');
            if isempty(sequenceConstructorHandle)
                error('%s.constructor is not a valid function on the path.',setSequence.className);
            end
            retObj=sequenceConstructorHandle(setSequence);
        end
    end

    methods
        function obj = sequence(setClassName, setUuid, setModel, setSimulations)
            %SEQUENCE
            %   Construct an instance of this class

            obj.className=setClassName;
            obj.uuid=setUuid;
            obj.hModel=setModel;
            obj.simulations=setSimulations;
        end

        function clearAll(obj)
            obj.simulations(1).clearAll();
        end
    end

    methods %getter/setter functions
        function obj=set.simulations(obj, setSimulations)
            if ~(isa(setSimulations,'mcmcInterface.simulation'))
                error('simulations is not mcmcInterface.simulation object');
            end
            obj.simulations=setSimulations;
        end

        function obj=set.className(obj, setType)
            if isempty(which(setType))
                error('type %s is not found on MATLAB path',setType);
            end
            obj.className=setType;
        end   
    end    
end

classdef empty < mcmcBayes.model
    %EMPTY
    %   Detailed explanation goes here
    
    properties
        
    end
    
    methods
        function obj = empty(setSubtype)
            %EMPTY 
            % Construct an instance of this class
            if nargin > 0
                superargs{1}='mcmcBayes.empty';%no group
                superargs{2}='mcmcBayes.empty';
                superargs{3}='';%no subtype
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.model(superargs{:});
        end

        function varElementId=getPhiVarElementId(obj)
            error('Should not get here.');
        end

        function [loglikelihood]=computeLogLikelihood(obj, data, samples)
            error('Should not get here.');
        end        
    end
end

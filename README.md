# mcmcBayes
**Warning: mcmcBayes is in the pre-release stage and is still being refined**

MATLAB based framework for running complex, MCMC-based studies in a repeatable manner.

# Dependencies

* [os-agnostic-utils](https://gitlab.com/reubenajohnston/os-agnostic-utils)
* [mcmcInterface](https://gitlab.com/mcmcbayes/mcmcinterface)
* models (e.g., see [regression](https://gitlab.com/mcmcbayes/regression))

# Installation

1. Make a `/sandboxes/<USERNAME>` folder
1. Clone the core repositories (mcmcBayes, mcmcInterface, and os-agnostic-utils) into `/sandboxes/<USERNAME>`
    ```
    $ git clone https://gitlab.com/mcmcbayes/mcmcbayes.git
    $ git clone https://gitlab.com/reubenajohnston/os-agnostic-utils.git mcmcbayes/os-agnostic-utils
    $ git clone https://gitlab.com/mcmcbayes/mcmcinterface.git mcmcbayes/mcmcinterface
    ```
1.  Perform the installation instructions for os-agnostic-utils
    * See os-agnostic-utils/README.md in os-agnostic-utils repository for instructions
1.  Perform the installation instructions for mcmcInterface 
    * See mcmcInterface/README.md in mcmcInterface repository for instructions
1.  Update MATLAB path to add folders in code/matlab
    ```
    >> addpath addpath /sandboxes/johnsra2/mcmcbayes/code/matlab
    ```
1.  Clone sequence repositories into sequences (or create new ones)
    * E.g., run the following to clone the powerPosterior sequence
        ```
        $ git clone https://gitlab.com/mcmcbayes/powerposterior.git
        ``` 
1.  Perform the installation instructions for any sequences that you have included
    * E.g., for the powerPosterior sequence, see README.md in the sequences's repository for instructions (i.e., sequences/powerposterior/README.md)
1.  Clone model repositories into models (or create new ones)
    * E.g., run the following to clone the regression model
        ```
        $ git clone https://gitlab.com/mcmcbayes/regression.git mcmcbayes/models/regression
        ```	
1.  Perform the installation instructions for any models that you have included
    * E.g., for the regression model, see README.md in the model's repository for instructions (i.e., models/regression/README.md)
	
# Usage

**For an example model that uses mcmcBayes, see [regression](https://gitlab.com/mcmcbayes/regression)** 

# Key definitions
* study-a group of sequences whose results can be generated together and presented
* sequence-a group simulations that are related and require special handling as a group (e.g., temperature runs in a power-posterior sequence)
* simulation-a single MCMC run
* model-abstract base class with functions to support model specific features (e.g., compute marginal log-likelihood for a model give dataset that is needed for power-posterior computations)
* group-optional instance of a model that is a higher level than individual and contains shared features for a collection of related individual models (to avoid duplication)
    * e.g., regression models
* individual-instance of a model that contains unique features for a model
    * e.g., linear regression model

# Recommended directory structure
```
/code-folder in the mcmcBayes repository with the MATLAB content
/studies
    /study1-clone of repository for study1
    /study2
    ...
/datasets
    /dataset1-clone of repository for dataset1
    /dataset2
    ...
/mcmcinterface-clone of mcmcInterface repository
/models
    /modelType1
    /modelType2
    ...
/os-agnostic-utils-clone of os-agnostic-utils repository
/sequences
    /sequenceType1
    /sequenceType2
```